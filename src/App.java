import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        ejer8();
    }
    public static void ejer4(){
        double velkmh=0;
        double velms=0;
        System.out.println("Ingrese una velocidad en Km/h: ");
        Scanner vel = new Scanner(System.in);
        velkmh=vel.nextDouble();
        velms=(velkmh*0.27777777777);
       // velms=velkmh;
        System.out.println("La velocidad ingresada en m/s es: \n"+velms);
        vel.close();
    }
    public static void ejer5(){
        double co;
        double ca;
        double hipo;
        System.out.println("Ingrese el valor del cateto opuesto: ");
        Scanner catetos = new Scanner(System.in);
        co=catetos.nextDouble();
        System.out.println("Ingrese el valor del cateto adyacente: ");
        ca=catetos.nextDouble();
        catetos.close();
        hipo=Math.sqrt(Math.pow(co, 2)+Math.pow(ca, 2));
        System.out.println(hipo);
    }
    public static void ejer6(){
        float num=0;
        System.out.println("Ingrese un número: ");
        Scanner multiplo= new Scanner(System.in);
        num=multiplo.nextFloat();
        multiplo.close();
        float num2=num%10;
        if(num2==0){
            System.out.println(num+" Es un multiplo de 10");
        }
        else{
            System.out.println(num+" No es un multiplo de 10");
        }
    }
    public static void ejer8(){
        long num1,num2;
        System.out.println("Ingrese el primer numero: ");
        Scanner compro= new Scanner(System.in);
        num1=compro.nextLong();
        System.out.println("Ingrese el segundo numero: ");
        num2=compro.nextLong();
        compro.close();
        if(num2!=0){
            System.out.println((num1/num2)+" Es el resultado de: "+num1+" dividido en"+ num2);
        }
        else{
            System.out.println(" No se puede realizar la división");
        }

    }
}
